# Get Help

## Questions or Suggestions?
Have questions or suggestions for additional documentation? Email [oitresearchsupport@duke.edu](). We can help you get started with research computing resources. For advanced topics, book office hours through [this link.](https://outlook.office365.com/owa/calendar/ResearchComputing@ProdDuke.onmicrosoft.com/bookings/)

## Meet the Interns

This site is produced and managed by the Research Computing Interns.

![Johnny Pesavento](../assets/img/johnny.jpg){ align=left  width="150" }

### Johnny Pesavento

Hi! I'm Johnny, and I am a freshman studying computer science and possibly economics. My main areas of expertise are Python (including Jupyter), web development, and git.

<br/>

![Amir Ergashev](../assets/img/amir.png){ align=left  width="150" }
### Amir Ergashev

Hi! My name is Amir and I'm also a freshman planning to study computer science, with possibly Russian. My main area of expertise is Java, with a bit of git and web development. In my free time, I enjoy playing ping pong and going to the gym.

![Clara Savchik](../assets/img/clara.jpg){ align=left  width="150" }
### Clara Savchik

Description  
Description  
Description 

<br/>
