# Getting Started with Jupyter Lab on OpenOnDemand

## Logging in 
1. Login to the Duke OpenOnDemand website [here](https://dcc-ondemand-01.oit.duke.edu)
2. Click on Interactive Apps in the top navigation menu
3. Click on Jupyter Lab

## Launching a Jupyter Lab server
1. For lab account, input the name of your DCC group (list of all groups can be found [here](https://dcc-ondemand-01.oit.duke.edu/pun/sys/dashboard/files/fs//hpc/group))
2. Under partition, type in "common"
3. Input the number of hours you would like the server to remain active (please try to remain small, as it will continue running even if you are not using it)
4. Input the desired amount of nodes, memory, and CPUs (try to start small with only a few gigabytes of memory and cores)
5. Enter any additional Slurm parameters (this is optional)
6. Press the blue "Launch" button on the buttom of the page

## Connecting to Jupyter 
1. After pressing the blue "launch" button, your job will be queued to start a Jupyter Lab server. You should see this automatically
2. Wait a few seconds to a few minutes for the Jupyter Lab server to finish launching. The status will automatically change from "Starting" to "Running" when the server is ready
3. Press the blue "Connect to Jupyter" button when the server is running to access your Jupyter Lab server

## Using Jupyter Lab
1. Click on Python3 under "Notebook" to create a new .ipynb notebook
2. Alternatively, upload your existing .ipynb files using the pane on the left-hand side
3. You can drag-and-drop or press the upward facing arrow to upload files
4. When you are ready you can run your Jupyter Notebook by pressing the run button at the top of the .ipynb file window