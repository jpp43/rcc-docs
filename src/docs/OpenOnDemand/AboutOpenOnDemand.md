# About Open OnDemand
Open OnDemand is an NSF-funded open-source HPC portal based on OSC’s original OnDemand portal. The goal of Open OnDemand is to provide an easy way for system administrators to provide web access to their HPC resources, including, but not limited to:

- Plugin-free web experience
- Easy file management
- Command-line shell access
- Job management and monitoring across different batch servers and resource managers
- Graphical desktop environments and desktop applications
