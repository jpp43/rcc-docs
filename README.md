# Duke Research Computing website

The [Duke Research Computing documentation website][url_docs] is generated from
[MarkDown][url_markdown] documents hosted in this repository.

## How to add a new document

1. Clone the repository to your local machine
2. Run `pip install -r requirements.txt` in the repository directory
3. Add a the document as a .md file somewhere in the `/src/docs` folder
4. Add the file name and path under the `nav` header at the bottom of `/mkdocs.yml`
5. Run `mkdocs serve` to view the updated website on your local machine
6. Commit and push to this repository, the public website will update automatically in ~5 minutes

[comment]: #  (link URLs -----------------------------------------------------)

[url_mkdocs]:           http://mkdocs.org
[url_markdown]:         https://en.wikipedia.org/wiki/Markdown
[url_docs]:             https://jpp43.pages.oit.duke.edu/rcc-docs/
